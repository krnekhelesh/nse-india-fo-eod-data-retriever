//  Copyright (C) 2019 Nekhelesh Ramananthan
//
//  This program is free software; you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation; either version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along with
//  this program; if not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.13
import QtQuick.Dialogs 1.2

import NSEIndia 1.0

ApplicationWindow {
    id: mainWindow

    visible: true
    width: 500; height: 550

    minimumWidth: 500
    minimumHeight: 550

    title: "NSE India F&O EOD Data Retriever v0.1"

    NSEIndia {
        id: nseIndia
        website: "https://www.nseindia.com/products/content/derivatives/equities/historical_fo.htm"
        onError: {
            errorPopup.errorMsg = error_msg
            errorPopup.open()
        }
    }

    DownloadPopup {
        id: downloadPopup
        anchors.centerIn: parent
        width: parent.width - 40; height: nseIndia.total_no_of_tasks == 0 ? 60 : 80
        totalTasks: nseIndia.total_no_of_tasks
        currentTask: nseIndia.current_task
    }

    ErrorPopup {
        id: errorPopup
        anchors.centerIn: parent
        width: parent.width - 40; height: 120
    }

    StackView {
        anchors.fill: parent

        initialItem: HomePage {
            nseIndiaController: nseIndia
        }
    }
}
