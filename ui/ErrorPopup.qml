//  Copyright (C) 2019 Nekhelesh Ramananthan
//
//  This program is free software; you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation; either version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along with
//  this program; if not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.13

Popup {
    id: errorPopup

    property string errorMsg: ''

    modal: true
    closePolicy: Popup.NoAutoClose

    Label {
        id: errorMsgLabel
        text: errorMsg
        wrapMode: Text.WordWrap
        anchors { top:parent.top; left: parent.left; right: parent.right; margins: 5 }
    }

    Button {
        text: 'Ok'
        width: 100
        onClicked: errorPopup.close()
        Material.background: Material.color(Material.Red, Material.Shade600)
        anchors { right: parent.right; bottom: parent.bottom; margins: 5 }
    }
}