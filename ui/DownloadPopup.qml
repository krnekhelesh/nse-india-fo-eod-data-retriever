//  Copyright (C) 2019 Nekhelesh Ramananthan
//
//  This program is free software; you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation; either version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along with
//  this program; if not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.13

Popup {
    id: downloadPopup

    property double totalTasks: 0
    property double currentTask: 0

    modal: true
    closePolicy: Popup.NoAutoClose
    contentItem: Column {
        anchors {
            left: parent.left; right: parent.right; top: parent.top; margins: 20
        }

        Item {
            height: 30
            anchors { left: parent.left; right: parent.right }
            Label {
                anchors { top: parent.top; left: parent.left }
                text: totalTasks != 0 ?
                      'Please wait until all the data is downloaded...'
                      : 'All stock data have been downloaded!'
            }

            Button {
                height: parent.height + 10
                visible: totalTasks == 0
                text: 'Close'
                Material.background: Material.color(Material.Green, Material.Shade600)
                anchors { top: parent.top; right: parent.right; topMargin: -10}
                onClicked: downloadPopup.close()
            }
        }

        ProgressBar {
            id: progressBar
            anchors { left: parent.left; right: parent.right }
            from: 0
            to: totalTasks
            value: currentTask
            padding: 1
            visible: from != 0 || to != 0

            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 6
                color: "#e6e6e6"
                radius: 3
            }

            contentItem: Item {
                implicitWidth: 200
                implicitHeight: 5

                Rectangle {
                    width: progressBar.visualPosition * parent.width
                    height: parent.height
                    radius: 2
                    color: "#17a81a"
                }
            }
        }
    }
}