//  Copyright (C) 2019 Nekhelesh Ramananthan
//
//  This program is free software; you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation; either version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along with
//  this program; if not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.13
import QtQuick.Dialogs 1.2

Page {

    property var nseIndiaController: None

    FileDialog {
        id: openDialog
        folder: shortcuts.home
        selectFolder: true
        onAccepted: {
            var folder_path = openDialog.folder
            outputFolderLabel.text = openDialog.folder
            nseIndiaController.download_dir = openDialog.folder
        }
    }

    Column {
        id: inputOptionsColumn
        spacing: 20
        anchors {
            left: parent.left;
            right: parent.right;
            top: parent.top;
            margins: 20;
        }

        Column {
            id: instrumentColumn
            width: parent.width
            Label { text: 'Instrument'; font.bold: true }
            ComboBox {
                id: instrumentComboBox
                model: [
                    'Select an Instrument',
                    'Index Futures',
                    'Index Options',
                    'Stock Options',
                    'Stock Futures',
                ]
                width: parent.width
                onActivated: {
                    if (instrumentComboBox.currentText == 'Index Options' ||
                        instrumentComboBox.currentText == 'Index Futures') {
                        symbolComboBox.enabled = true
                        symbolComboBox.model = [
                            'Select symbol',
                            'NIFTY 50',
                            'NIFTY MIDCAP 50',
                            'BANK NIFTY',
                            'NIFTY INFRA',
                            'NIFTY IT',
                            'NIFTY PSE',
                            'NIFTY CPSE'
                        ]
                    } else if (instrumentComboBox.currentText == 'Stock Options' ||
                               instrumentComboBox.currentText == 'Stock Futures') {
                        symbolComboBox.enabled = true
                        symbolComboBox.model = [
                            'Select symbol',
                            'ACC',
                            'AMBUJACEM',
                            'BAJAJ-AUTO',
                            'HDFCBANK',
                            'HINDALCO',
                            'HINDUNILVR',
                            'HDFC',
                            'ICICIBANK',
                            'ITC',
                            'L&T',
                            'RIIL',
                            'SBIN',
                            'TATAMOTORS',
                            'TATAPOWER',
                            'TATASTEEL'
                        ]
                    } else {
                        optionTypeComboBox.enabled = true
                        symbolComboBox.enabled = false
                        symbolComboBox.model = ['Select symbol']
                    }

                    if (instrumentComboBox.currentText == 'Index Futures' ||
                        instrumentComboBox.currentText == 'Stock Futures' ||
                        instrumentComboBox.currentText == 'Select an Instrument') {
                        optionTypeComboBox.enabled = false
                        optionTypeComboBox.currentIndex = 0
                    } else {
                        optionTypeComboBox.enabled = true
                    }
                }
            }
        }

        Column {
            id: symbolColumn
            width: parent.width
            Label { text: 'Symbol'; font.bold: true }
            ComboBox {
                id: symbolComboBox
                enabled: false
                model: ['Select symbol']
                width: parent.width
            }
        }

        Column {
            id: optionTypeColumn
            width: parent.width
            Label { text: 'Option Type'; font.bold: true }
            ComboBox {
                id: optionTypeComboBox
                enabled: false
                model: ['Select an Option Type', 'CE', 'PE']
                width: parent.width
            }
        }

        Column {
            id: yearColumn
            width: parent.width
            Label { id: yearLabel; text: 'Year'; font.bold: true }
            RangeSlider {
                id: yearSlider
                from: 2000
                to: 2024
                first.value: 2019
                second.value: 2024
                snapMode: RangeSlider.SnapAlways
                stepSize: 1
                width: parent.width

                ToolTip {
                    parent: yearSlider.first.handle
                    visible: yearSlider.first.pressed
                    text: yearSlider.first.value
                }

                ToolTip {
                    parent: yearSlider.second.handle
                    visible: yearSlider.second.pressed
                    text: yearSlider.second.value
                }

                Connections {
                    target: yearSlider.first
                    onValueChanged: yearLabel.text = 'Year (' + yearSlider.first.value + ' - ' + yearSlider.second.value + ')'
                }

                Connections {
                    target: yearSlider.second
                    onValueChanged: yearLabel.text = 'Year (' + yearSlider.first.value + ' - ' + yearSlider.second.value + ')'
                }
            }
        }

        Column {
            width: parent.width
            Label { text: 'Download Directory'; font.bold: true }
            Row {
                width: parent.width
                spacing: 10

                TextField {
                    id: outputFolderLabel
                    width: parent.width - parent.spacing - setOutputFolderButton.width
                    placeholderText: 'Output Folder'
                    readOnly: true
                }

                Button {
                    id: setOutputFolderButton
                    text: 'Choose Folder'
                    onClicked: openDialog.open()
                }
            }
        }

        Button {
            text: 'Fetch Data'
            width: parent.width / 3
            anchors.horizontalCenter: parent.horizontalCenter
            Material.background: Material.color(Material.Green, Material.Shade600)
            enabled: true

            ToolTip.delay: 500
            ToolTip.timeout: 3000
            ToolTip.visible: hovered
            ToolTip.text: "Fetch data from NSE India Website"

            onClicked: {
                // TODO: Do a better input verification
                if (instrumentComboBox.currentIndex == 0) {
                    return
                }

                nseIndiaController.get_stock_data_thread(
                    instrumentComboBox.currentText,
                    symbolComboBox.currentText,
                    yearSlider.first.value,
                    yearSlider.second.value,
                    optionTypeComboBox.currentText
                )

                downloadPopup.open()
            }
        }
    }
}