#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3 of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtCore import QObject, pyqtProperty, pyqtSlot, pyqtSignal
from selenium.common.exceptions import NoSuchElementException

from controllers.backgroundworker import BackgroundWorker
from modules.nseindia import NSEIndia


class NSEIndiaController(QObject):
    error = pyqtSignal('QString', arguments=['error_msg'])
    currentTaskChanged = pyqtSignal()
    totalTasksChanged = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self._website_url = None
        self._download_dir = None
        self._nseIndia = NSEIndia()
        self._total_no_of_tasks = 0
        self._current_task = 0

    @pyqtProperty('QString')
    def website(self):
        return self._website_url

    @website.setter
    def website(self, value):
        self._website_url = value
        self._nseIndia.website = self._website_url

    @pyqtProperty('QString')
    def download_dir(self):
        return self._download_dir

    @download_dir.setter
    def download_dir(self, value):
        if 'file://' in value:
            # TODO: Find a better way of removing file:// from the path received
            #   from the UI (QML).
            self._download_dir = value.replace('file://', '')
        else:
            self._download_dir = value
        self._nseIndia.download_dir = self._download_dir

    @pyqtProperty(float, notify=totalTasksChanged)
    def total_no_of_tasks(self):
        return self._total_no_of_tasks

    @pyqtProperty(float, notify=currentTaskChanged)
    def current_task(self):
        return self._current_task

    @pyqtSlot()
    def open_browser(self):
        self._nseIndia.open_browser()

    @pyqtSlot('QString')
    def load_website(self, wait_until_elem_id):
        self._nseIndia.get_website(timeout=5, wait_until_elem_id=wait_until_elem_id)

    def update_progress(self, value):
        self._current_task += value
        self.currentTaskChanged.emit()

    def get_stock_data(
        self,
        instrument_type: str,
        symbol_type: str,
        start_year: int,
        end_year: int,
        option_type: str,
    ) -> None:

        self._total_no_of_tasks = len(range(start_year, end_year + 1)) * 12
        self.totalTasksChanged.emit()

        self._current_task = 0
        self.currentTaskChanged.emit()

        self.open_browser()
        self.load_website("instrumentType")

        for year in range(start_year, end_year + 1):
            try:
                self._nseIndia.get_stock_data(
                    instrument_type,
                    symbol_type,
                    year,
                    option_type,
                    self.update_progress,
                )
            except (TimeoutError, NoSuchElementException) as error_msg:
                self.error.emit(str(error_msg))

        self.quit()

        self._current_task = self._total_no_of_tasks = 0
        self.currentTaskChanged.emit()
        self.totalTasksChanged.emit()

    @pyqtSlot('QString', 'QString', int, int, 'QString')
    def get_stock_data_thread(
        self,
        instrument_type: str,
        symbol_type: str,
        start_year: int,
        end_year: int,
        option_type: str,
    ) -> None:
        self.get_stock_thread = BackgroundWorker(
            lambda: self.get_stock_data(
                instrument_type, symbol_type, start_year, end_year, option_type
            )
        )
        self.get_stock_thread.start()

    @pyqtSlot()
    def quit(self):
        self._nseIndia.quit()
