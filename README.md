## National Stock Exchange (India) - F&O EOD Data Retriever

This is a simple desktop app that is intended to automate the retrieval of equity
futures and options index and stock data from the National Stock Exchange India web
portal.

![image](screenshots/main-window.png)