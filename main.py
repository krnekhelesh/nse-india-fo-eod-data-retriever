#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3 of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, see <http://www.gnu.org/licenses/>.

import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlApplicationEngine, qmlRegisterType

from controllers.nseindiacontroller import NSEIndiaController

import resources

app = QGuiApplication(sys.argv)
app.setOrganizationName("Nekhelesh")
app.setOrganizationDomain("Nekhelesh")
app.setApplicationName("NSE India F&O EOD Data Retriever")

qmlRegisterType(NSEIndiaController, 'NSEIndia', 1, 0, 'NSEIndia')

engine = QQmlApplicationEngine(parent=app)
engine.load(QUrl('ui/main.qml'))
engine.quit.connect(app.quit)

sys.exit(app.exec_())
