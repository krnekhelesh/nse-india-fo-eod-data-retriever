#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3 of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, see <http://www.gnu.org/licenses/>.

import calendar
import os
import shutil
import time
from datetime import datetime, timedelta
from typing import Optional, List, Callable

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.common.exceptions import NoSuchElementException


class NSEIndia:
    def __init__(self):
        self._bot = None
        self._website_url = ''
        self._download_dir = ''
        self._profile = ''
        self.setup_profile()

    @property
    def website(self):
        return self._website_url

    @website.setter
    def website(self, value):
        self._website_url = value

    @property
    def download_dir(self):
        return self._download_dir

    @download_dir.setter
    def download_dir(self, download_dir):
        self._download_dir = download_dir
        self._profile.set_preference("browser.download.folderList", 2)
        self._profile.set_preference("browser.download.dir", download_dir)

    def setup_profile(self):
        # Set Firefox profile preferences such as not showing a download
        # prompt and setting custom download location
        self._profile = FirefoxProfile()
        self._profile.set_preference("browser.download.panel.shown", False)
        self._profile.set_preference(
            "browser.helperApps.neverAsk.openFile",
            "application/csv,application/vnd.ms-excel",
        )
        self._profile.set_preference(
            "browser.helperApps.neverAsk.saveToDisk",
            "application/csv,application/vnd.ms-excel",
        )

    def open_browser(self):
        self._bot = webdriver.Firefox(
            executable_path="./geckodriver", firefox_profile=self._profile
        )

    def get_website(
        self, timeout: int, wait_until_elem_id: Optional[str] = None
    ) -> None:
        self._bot.get(self._website_url)
        if wait_until_elem_id is not None:
            WebDriverWait(self._bot, timeout=timeout).until(
                EC.presence_of_element_located((By.ID, "instrumentType"))
            )
        else:
            time.sleep(timeout)

    def quit(self):
        self._bot.quit()

    def scroll_page(self, direction: str) -> None:
        html = self._bot.find_element_by_tag_name("html")
        if direction == "DOWN":
            html.send_keys(Keys.PAGE_DOWN)
        else:
            html.send_keys(Keys.PAGE_UP)
        time.sleep(0.5)

    def _set_instrument_type(self, instrument_type: str) -> None:
        instrument_select = Select(self._bot.find_element_by_id("instrumentType"))
        instrument_select.select_by_visible_text(instrument_type)

    def _set_symbol_type(self, symbol_type: str) -> None:
        symbol_select = Select(self._bot.find_element_by_id("symbol"))
        symbol_select.select_by_visible_text(symbol_type)

    def _set_year(self, year: str) -> None:
        year_select = Select(self._bot.find_element_by_id("year"))
        year_select.select_by_visible_text(year)

    def _set_expiry(self, date: datetime) -> None:
        expiry_select = Select(self._bot.find_element_by_id("expiryDate"))
        expiry_select.select_by_visible_text(date.strftime("%d-%m-%Y"))

    def _set_option_type(self, option_type: str) -> None:
        option_type_select = Select(self._bot.find_element_by_id("optionType"))
        option_type_select.select_by_visible_text(option_type)

    def _set_from_to_time_period(self, from_date: datetime, to_date: datetime) -> None:
        time_period_radio_btn = self._bot.find_element_by_id("rdDateToDate")
        time_period_radio_btn.click()

        from_date_input = self._bot.find_element_by_id("fromDate")
        from_date_input.send_keys(from_date.strftime("%d-%m-%Y"))

        to_date_input = self._bot.find_element_by_id("toDate")
        to_date_input.send_keys(to_date.strftime("%d-%m-%Y"))

    def _clear_from_to_time_period(self) -> None:
        from_date_input = self._bot.find_element_by_id("fromDate")
        from_date_input.clear()

        to_date_input = self._bot.find_element_by_id("toDate")
        to_date_input.clear()

    def _get_data(self) -> None:
        get_data_btn = self._bot.find_element_by_id("getButton")
        get_data_btn.click()

    def _download_data(self) -> None:
        download_csv_link = self._bot.find_element_by_class_name("download-data-link")
        download_csv_link.click()

    def _get_expiry_dates(self) -> List[str]:
        expiry_select = Select(self._bot.find_element_by_id("expiryDate"))
        return [
            expiry.get_attribute("value")
            for expiry in expiry_select.options
            if expiry.get_attribute("value") != "select"
        ]

    @staticmethod
    def _get_month_expiry_dates(expiry_dates: List[datetime]) -> List[datetime]:
        month_expiry_dates = []
        target_month = None
        largest_date = None
        for date in expiry_dates:
            if target_month is None and largest_date is None:
                target_month = date.month
                largest_date = date
                continue

            if date > largest_date:
                if date.month != target_month:
                    target_month = date.month
                    if largest_date - timedelta(days=1) in expiry_dates:
                        month_expiry_dates.append(largest_date - timedelta(days=1))
                    else:
                        month_expiry_dates.append(largest_date)
                largest_date = date

        if expiry_dates[-1] - timedelta(days=1) in expiry_dates:
            month_expiry_dates.append(expiry_dates[-1] - timedelta(days=1))
        else:
            month_expiry_dates.append(expiry_dates[-1])

        return month_expiry_dates

    @staticmethod
    def _get_last_friday(year: int, month: int):
        cal = calendar.Calendar(firstweekday=calendar.MONDAY)
        month_cal = cal.monthdatescalendar(year, month)
        return [
            day
            for week in month_cal
            for day in week
            if day.weekday() == calendar.FRIDAY and day.month == month
        ][-1]

    def get_stock_data(
        self,
        instrument_type: str,
        symbol_type: str,
        year: int,
        option_type: str,
        update_progress: Optional[Callable] = None,
    ) -> None:
        self._set_instrument_type(instrument_type)
        self._set_symbol_type(symbol_type)
        self._set_year(str(year))

        if 'Futures' not in instrument_type:
            self._set_option_type(option_type)

        expiry_dates = self._get_expiry_dates()
        expiry_dates = list(
            map(lambda date: datetime.strptime(date, "%d-%m-%Y"), expiry_dates)
        )

        month_expiry_dates = self._get_month_expiry_dates(expiry_dates)

        for index, expiry_date in enumerate(month_expiry_dates):
            if index == 0:
                from_date = self._get_last_friday(year - 1, 12)
            else:
                from_date = month_expiry_dates[index - 1] + timedelta(days=1)

            to_date = expiry_date

            self._set_expiry(to_date)
            self._set_from_to_time_period(from_date, to_date)
            self._get_data()

            # WebDriverWait(self._bot, timeout=5).until(
            #     EC.presence_of_element_located((By.CLASS_NAME, "download-data-link"))
            # )

            time.sleep(10)

            self.scroll_page(direction="DOWN")

            try:
                self._download_data()
            except NoSuchElementException:
                print(
                    f'No data available for {from_date} - {to_date} for {symbol_type}!'
                )

            self._clear_from_to_time_period()

            if update_progress is not None:
                update_progress(1)

        if 'Futures' not in instrument_type:
            stock_dir = os.path.join(
                self._download_dir,
                f"{year}_{symbol_type.replace(' ', '_')}_{option_type}"
            )
        else:
            stock_dir = os.path.join(
                self._download_dir,
                f"{year}_{symbol_type.replace(' ', '_')}"
            )

        os.mkdir(stock_dir)

        for file in os.listdir(self._download_dir):
            if file.lower().endswith('.csv'):
                shutil.move(
                    os.path.join(self._download_dir, file),
                    os.path.join(stock_dir, file),
                )
