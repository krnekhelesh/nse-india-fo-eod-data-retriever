## Setting up the development environment

### 1. Install Dependencies to a Virtual Environment

This helps avoid package version conflicts and isolates the dependencies 
of this project from others.

```bash
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

### 2. Download GeckoDriver

This project uses `Selenium` to automate the download of stock data from 
NSE's website. Currently, the Firefox webdriver is hardcoded in the code.
As a result, the selenium webdriver for Firefox need to included in the
project root directory. The latest webdriver can be found [here](https://github.com/mozilla/geckodriver/releases).

### 3. Run Project

```bash
python main.py
```